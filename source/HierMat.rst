HierMat package
===============

Submodules
----------

HierMat.block_cluster_tree module
---------------------------------

.. automodule:: HierMat.block_cluster_tree
    :members:
    :undoc-members:
    :show-inheritance:

HierMat.cluster module
----------------------

.. automodule:: HierMat.cluster
    :members:
    :undoc-members:
    :show-inheritance:

HierMat.cluster_tree module
---------------------------

.. automodule:: HierMat.cluster_tree
    :members:
    :undoc-members:
    :show-inheritance:

HierMat.cuboid module
---------------------

.. automodule:: HierMat.cuboid
    :members:
    :undoc-members:
    :show-inheritance:

HierMat.grid module
-------------------

.. automodule:: HierMat.grid
    :members:
    :undoc-members:
    :show-inheritance:

HierMat.hmat module
-------------------

.. automodule:: HierMat.hmat
    :members:
    :undoc-members:
    :show-inheritance:

HierMat.rmat module
-------------------

.. automodule:: HierMat.rmat
    :members:
    :undoc-members:
    :show-inheritance:

HierMat.splitable module
------------------------

.. automodule:: HierMat.splitable
    :members:
    :undoc-members:
    :show-inheritance:

HierMat.utils module
--------------------

.. automodule:: HierMat.utils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: HierMat
    :members:
    :undoc-members:
    :show-inheritance:
