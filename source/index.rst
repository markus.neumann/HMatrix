.. Hierarchical Matrices documentation master file, created by
   sphinx-quickstart on Thu Mar 23 11:20:05 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Hierarchical Matrices's documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Documentation
=============

This package is the result of my master thesis at the `Institute of Mathematics, University of Zurich`_.

It is manly based on :cite:`hackbusch2015hierarchical`.

The whole package including this documentation can be found at this `Git Repository`_

:Author: `Markus Neumann`_

:Date: |today|

:Supervisor: `Prof. Dr. Stefan Sauter`_

Links and references
--------------------

.. rubric:: Links

.. _Institute of Mathematics, University of Zurich: http://www.math.uzh.ch/index.php?&L=1

`Institute of Mathematics, University of Zurich`_

.. _Prof. Dr. Stefan Sauter: http://www.math.uzh.ch/index.php?professur&key1=105&L=1

`Prof. Dr. Stefan Sauter`_

.. _Git Repository: https://git.math.uzh.ch/markus.neumann/HMatrix

`Git Repository`_

.. _Markus Neumann: markus.neumann@math.uzh.ch

.. rubric:: References

.. bibliography:: thesis.bib
